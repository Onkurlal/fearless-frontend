import "./App.css";
import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import PresentatonForm from "./PresentationForm";
import MainPage from "./MainPage";
import AttendConferenceForm from "./AttendConferenceForm";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App({ attendees }) {
  if (attendees === undefined) {
    return null;
  }
  return (
    <>
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="attendees">
              <Route index element={<AttendeesList attendees={attendees} />} />
              <Route path="new" element={<AttendConferenceForm />} />
            </Route>
            <Route index element={<MainPage />} />
            <Route path="presentations">
              <Route path="new" element={<PresentatonForm />} />
            </Route>
            <Route path="conferences">
              <Route path="new" element={<ConferenceForm />} />
            </Route>
            <Route path="locations">
              <Route path="new" element={<LocationForm />} />
            </Route>
          </Routes>
        </div>
      </BrowserRouter>
    </>
  );
}

export default App;
